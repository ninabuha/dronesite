 <?php

if($_SERVER["REQUEST_METHOD"]=="POST"){

	$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
	$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
	$phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
	$message = filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING);

	if(!empty($name) && !(empty($email) || empty($phone)) && !empty($message) ){

		$message = "Name: " . $name . "\r\n";
		$message .= "Email:" . $email . "\r\n";
		$message .= "Phone: " . $phone . "\r\n";
		$message .= "Message: " . $message;
		$message = wordwrap($message, 70);


		try{
		     mail('info@sd-operations.com', 'Poruka sa sajta: Drone | SDO-corp', $message);
		     echo '<script>
		                alert("Poruka je uspešno poslata.")
		                window.history.back();
		            </script>';
		}
		catch(Exception $e){
		    echo '<script>
		                alert("Neuspešno poslata poruka.")
		                window.history.back();
		            </script>';
		}
	}else{

		echo '<script>
		                alert("Neuspešno poslata poruka.")
		                window.history.back();
		            </script>';
	}

	

}else{

	echo '<script>
            alert("Neuspešno poslata poruka.")
            window.history.back();
          </script>';

}



 ?>
