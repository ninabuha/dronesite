(function ($) {
  $(document).ready(function(){
    
	var nav = $('.fixed-top').css("background", "none");
	nav.css("border-bottom", "none");
	
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 350) {
				nav.css("background-color", "#313336");
				nav.css("border-bottom", "5px solid orange");
			} else {
				nav.css("background", "none");
				nav.css("border-bottom", "none");
			}
		});

	
	});

});
  }(jQuery));